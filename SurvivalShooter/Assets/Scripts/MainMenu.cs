﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour {

	public Slider slider;
	public GameObject loadingScreen;
	public TextMeshProUGUI progressText;

	public void PlayGame() {
		StartCoroutine (LoadAsynchronously ());
	}

	IEnumerator LoadAsynchronously() {
		AsyncOperation operation = SceneManager.LoadSceneAsync ("Level1");

		gameObject.SetActive (false);
		loadingScreen.SetActive (true);

		while (!operation.isDone) {
			float progress = Mathf.Clamp01 (operation.progress / 0.9f);
			Debug.Log ("Progress: " + progress);
			slider.value = progress;
			progressText.text = progress * 100f + "%";

			yield return operation;
		}
	}

	public void QuitGame() {
		Debug.Log ("Quit!");
		Application.Quit ();
	}
}
